from gate_api import ApiClient, Configuration, Order, SpotApi
from credentials import GATE_API_KEY
from pycoingecko import CoinGeckoAPI

import time
from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json

class Parser:
    def __init__(self):
        # Initialize API client
        try:
            config = Configuration(
                key=GATE_API_KEY
            )
            self.spot_api = SpotApi(ApiClient(config))
        except Exception as e:
            print("Gate io login failed,", str(e))


    def get_binance_tickers(self):
        """
        returns set of binance tickers according to coingecko
        """
        cg = CoinGeckoAPI()
        binance_tickers = set()
        for i in range(12):
            tickers = cg.get_exchanges_tickers_by_id(id='binance', page=i)
            for ticker in tickers['tickers']:
                binance_tickers.add(ticker['base'])
        return binance_tickers

    def get_gate_tickers(self):
        """
        returns gate tickers according to gate api
        """
        curr_list = self.spot_api.list_currencies()
        gate_tickers = set()
        for currency in curr_list:
            if not currency.delisted and not currency.trade_disabled and not any(True for symbol in ['_', '5L', '3L', '3S', '5S', 'BEAR', 'BULL'] if symbol in currency.currency):
                gate_tickers.add(currency.currency)

        return gate_tickers

    def update_tickers(self):
        """
        updates tickers for parsing
        """
        gate_tickers = self.get_gate_tickers()
        binance_tickers = self.get_binance_tickers()

        self.tickers = gate_tickers.difference(binance_tickers)

    def get_prices(self):
        gate_tickers = self.spot_api.list_tickers()
        t = int(time.time())
        prices = list()
        for ticker in gate_tickers:
            if ticker.currency_pair.split('_')[1] == 'USDT' and ticker.currency_pair.split('_')[0] in self.tickers:
                prices.append({ticker.currency_pair.split('_')[0]:{"price":ticker.last, "time":t}})

        return prices